# ![git](/img/git-logo.svg) <img src="img/bigshop-logo.svg" align="right">

## Configurações

- `git config --global user.name "Seu Nome Aqui"`: Define o seu nome dentro do sistema git (O comando é necessário apenas uma vez após a instalação)

- `git config --global user.email "seuemail@aqui.com"`:  Define o seu email dentro do sistema git (O comando é necessário apenas uma vez após a instalação)

- `git config --list`: Lista todas as configurações

## O básico

- `git init`: Inicia o controle de versão dentro do diretório atual (todo o versionamento do código é armazenado dentro de um diretório oculto chamado .git)

- `git status`: Exibe o status dos arquivos. 

    - Mas quais são os status e o que cada um significa? 

        - *untracked*: O git não está controlando as versões do arquivo. 
        (Working directory)

        - *unmodified*: O arquivo não foi modificado se comparado ao último commit.

        - *modified*: O arquivo foi modificado se comparado ao último commit 

        - *staged*: O arquivo foi adicionado ao controle de versão e fará parte do próximo commit. (Staged area)


- `git add nomedoarquivo.php `: Adiciona as alterações do arquivo ou um novo arquivo para a Staged area. (Para adicionar todos os novos arquivos ou arquivos modificados, basta utilizar o comando git add .)


- `git commit -m "descrição da alteração no arquivo"`: Envia todas as alterações nos arquivos que estão na staged area (status staged) para o diretório local de versionamento (diretório .git)

- `git restore --staged nomedoarquivo.php`: Volta o arquivo que estava na Staged area (staged)  para o Working directory (untracked)

- `git log`: Exibe o id, autor, a data e o que foi feito (a descrição do commit) em todos os commits da branch.  

- `git checkout iddocommit`: Acessa os arquivos do commit selecionado.

- `git diff`: Exibe um comparativo do que era o que foi modificado.

- `git push`: Envia todo o projeto commitado para a nuvem.

- `git pull`: Atualiza o projeto local com a última versão que está na nuvem


## Branches

- `git branch`: Exibe todas as branches do projeto.

- `git checkout -b nomedabranch`: Cria uma branch.

- `git merge nomedabranch`: Leva todas as alterações da branch criada para a branch master.

- `git push origin nomedabranch`: Envia todo a branch para a nuvem.

## Para se aprofundar 
- <a href="https://www.youtube.com/watch?v=NzskvPcp8tg"> [BIGBANG] Best Practices - GIT </a> - Um ótimo vídeo sobre boas práticas utilizando git flow.

- <a href="https://big-bang-shop.github.io/best-practices/docs/github/index.html">[BIGBANG] Best practices - GIT </a> Uma documentação sobre as boas práticas (em desenvolvimento) mantida pela Big Bang Digital.
<br>
<br>
Feito com muito ☕ & ❤️ para nós 👨🏻‍💻
